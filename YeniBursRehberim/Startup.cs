﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(YeniBursRehberim.Startup))]
namespace YeniBursRehberim
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
